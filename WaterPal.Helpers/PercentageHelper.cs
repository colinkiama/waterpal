﻿using System;

namespace WaterPal.Helpers
{
    public static class PercentageHelper
    {
        public static string GeneratePercentageString(double progressValue)
        {
            // When perfoming double -> int cast, 
            // decimal values are removed.
            return $"{(int)progressValue}% complete";
        }
    }
}
