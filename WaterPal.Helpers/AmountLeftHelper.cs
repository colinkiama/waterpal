﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WaterPal.Helpers
{
    public static class AmountLeftHelper
    {
        public static string GenerateAmountLeftString(object stringToUse)
        {
            return $"{stringToUse} left";
        }
    }
}
