﻿using System;
using WaterPal.Structs;

namespace WaterPal.Model
{
    public class WaterLog
    {
        public Measurement LoggedAmount { get; set; }
        public DateTime TimeOfLog { get; set; }

        public WaterLog(){}

        public WaterLog(Measurement loggedAmount)
        {
            LoggedAmount = loggedAmount;
            TimeOfLog = DateTime.UtcNow;
        }

        public override string ToString()
        {
            return $"{LoggedAmount}, {TimeOfLog}";
        }

    }
}
