﻿using System;
using WaterPal.Enums;

namespace WaterPal.Helpers
{
    public static class UnitHelper
    {
        const string FlozString = "fl oz";
        const string MlString = "ml";

        public static string GenerateStringForUnit(Unit unitUsed)
        {
            string stringToReturn;
            switch (unitUsed)
            {
                
                case Unit.floz:
                    stringToReturn = FlozString;
                    break;
                default:
                    // default will be used for ml
                    stringToReturn = MlString;
                    break;

            }

            return stringToReturn;
        }
    }
}
