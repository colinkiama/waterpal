﻿using System;
using WaterPal.Enums;
using WaterPal.Helpers;

namespace WaterPal.Structs
{
    public struct Measurement
    {
        // US fl oz
        // Using double instead of float to prevent errors in calculations
        const double MlPerFloz = 29.5735;
        public double Amount { get; set; }
        public Unit UnitOfMeasurement { get; set; }


        /// <summary>
        /// Every measurement will also store their amount in ml here.
        /// This way, it's easy to compare measurements with different units.
        /// </summary>
        public readonly double _universalAmount;

        public Measurement(double amount, Unit unitOfMeasurement)
        {
            Amount = amount;
            UnitOfMeasurement = unitOfMeasurement;
            if (unitOfMeasurement == Unit.floz)
            {
                _universalAmount = amount * MlPerFloz;
            }
            else
            {
                _universalAmount = amount;
            }
        }
        public override bool Equals(object obj)
        {
            return obj.GetHashCode() == this.GetHashCode();
        }

        public override int GetHashCode()
        {
            return _universalAmount.GetHashCode();
        }

        public static bool operator ==(Measurement left, Measurement right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(Measurement left, Measurement right)
        {
            return !(left == right);
        }        

        public static Measurement operator +(Measurement left, Measurement right)
        {
            Measurement measurementToReturn;
            // We want to use the newest unit value in this case.
            Unit unitToUse = DetermineUnitToUse(right);
            double amount = left._universalAmount + right._universalAmount;
            
            if (unitToUse == Unit.floz)
            {
                amount /= MlPerFloz;
            }
            measurementToReturn = new Measurement(amount, unitToUse);
            return measurementToReturn;
        }


        public static Measurement operator -(Measurement left, Measurement right)
        {
            Measurement measurementToReturn;
            
            // We want to keep the old unit value in this case
            Unit unitToUse = DetermineUnitToUse(left);
            double amount = left._universalAmount - right._universalAmount;

            if (unitToUse == Unit.floz)
            {
                amount /= MlPerFloz;
            }
            measurementToReturn = new Measurement(amount, unitToUse);
            return measurementToReturn;
        }

       


        private static Unit DetermineUnitToUse(Measurement measurementToUse)
        {
            Unit unitToReturn;
            
            if (measurementToUse.UnitOfMeasurement == Unit.floz)
            {
                unitToReturn = Unit.floz;
            }
            else unitToReturn = Unit.ml;

            return unitToReturn;
        }

        public override string ToString()
        {
            return $"{Amount} {UnitHelper.GenerateStringForUnit(UnitOfMeasurement)}";
        }
    }
}
