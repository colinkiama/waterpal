﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaterPal.Services;

namespace WaterPal.UWP.Services
{
    internal class NotificationService : INotificationService
    {
        private static Lazy<NotificationService> _lazy = 
            new Lazy<NotificationService>(() => new NotificationService());

        public static NotificationService Instance = _lazy.Value;
        private NotificationService() { }
        public void SendGoalNotification()
        {
            throw new NotImplementedException();
        }

        public void SendMilestoneNotification()
        {
            throw new NotImplementedException();
        }
         
        public bool SetReminderNotification(TimeSpan reminderInterval)
        {
            throw new NotImplementedException();
        }
    }
}
