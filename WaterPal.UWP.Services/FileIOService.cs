﻿using Microsoft.Toolkit.Uwp.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaterPal.Model;
using WaterPal.Services;
using Windows.Storage;
using Windows.Storage.Streams;

namespace WaterPal.UWP.Services
{
    public class FileIOService : IFileService
    {
        private static Lazy<FileIOService> _lazy =
            new Lazy<FileIOService>(() => new FileIOService());

        public static FileIOService Instance = _lazy.Value;

        private FileIOService() { }

        ApplicationDataContainer _localSettings = ApplicationData.Current.LocalSettings;
        StorageFolder _localFolder = ApplicationData.Current.LocalFolder;
        const string CurrentDayFileName = "currentFile.json";
        const string CurrentDateSettingsKey = "CurrentDate";
        LocalObjectStorageHelper localObjectHelper = new LocalObjectStorageHelper();

        /// <summary>
        /// Configures save data based on the last stored
        /// current day in the local settings.
        /// </summary>
        /// <returns></returns>
        public async Task CheckAndUpdateForCurrentDayAsync()
        {
            bool hasKeyBeenSet = localObjectHelper.KeyExists(CurrentDateSettingsKey);
            // 1) If local setting for day has not been set, create the save file then set
            // the current date
            if (!hasKeyBeenSet)
            {
                await CreateCurrentDayFileAsync();
                localObjectHelper.Save(CurrentDateSettingsKey, DateTime.UtcNow);
            }
            // 2) If local setting for day has been set but is outdated, update the date and 
            // clear the save file
            else
            {
                DateTime storedDateTime = localObjectHelper.Read(CurrentDateSettingsKey, DateTime.MinValue);
                // By doing this, the times should both be at the same value so we can compare
                // the dates without the times affecting the calculation.
                if (DateTime.UtcNow.Date > storedDateTime.Date)
                {
                    await ClearCurrentDayLogsAsync();
                    localObjectHelper.Save(CurrentDateSettingsKey, DateTime.UtcNow);
                }
            }
        }

        public async Task<ObservableCollection<WaterLog>> LoadCurrentDayLogAsync()
        {
            ObservableCollection<WaterLog> logs = null;
            var currentDayFile = await GetCurrentDayFileAsync();
            if (currentDayFile != null)
            {

                string currentDayLogsJson = await LoadStringDataFromFile(currentDayFile);
                logs = JsonConvert.DeserializeObject<ObservableCollection<WaterLog>>(currentDayLogsJson);
            }

            return logs;
        }

        private async Task<string> LoadStringDataFromFile(StorageFile fileToLoadFrom)
        {
            string contentToReturn = "";
            if (fileToLoadFrom != null)
            {
                var stream = await fileToLoadFrom.OpenAsync(Windows.Storage.FileAccessMode.Read);
                ulong size = stream.Size;
                using (var inputStream = stream.GetInputStreamAt(0))
                {
                    using (var dataReader = new DataReader(inputStream))
                    {
                        uint numBytesLoaded = await dataReader.LoadAsync((uint)size);
                        contentToReturn = dataReader.ReadString(numBytesLoaded);
                    }
                }

            }
            return contentToReturn;
        }

        public async Task SaveCurrentDayLogAsync(ObservableCollection<WaterLog> waterLogs)
        {
            var waterLogJson = JsonConvert.SerializeObject(waterLogs);
            await SaveLogsToFileAsync(waterLogJson);
        }

        private async Task SaveLogsToFileAsync(string waterLogsJson)
        {

            StorageFile fileToSave = await GetCurrentDayFileAsync();
            if (fileToSave == null)
            {
                fileToSave = await CreateCurrentDayFileAsync();
            }

            await SaveStringDataToFileAsync(fileToSave, waterLogsJson);
        }

        private async Task SaveStringDataToFileAsync(StorageFile fileToSave, string dataToWrite)
        {
            try
            {
                using (var stream = await fileToSave.OpenAsync(FileAccessMode.ReadWrite))
                {
                    using (var outputStream = stream.GetOutputStreamAt(0))
                    {
                        using (var dataWriter = new DataWriter(outputStream))
                        {
                            dataWriter.WriteString(dataToWrite);
                            await dataWriter.StoreAsync();
                            await outputStream.FlushAsync();
                            Debug.WriteLine($"{CurrentDayFileName} saved!");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }



        private async Task<StorageFile> CreateCurrentDayFileAsync()
        {
            return await _localFolder.CreateFileAsync(CurrentDayFileName, CreationCollisionOption.OpenIfExists);
        }

        private async Task<StorageFile> GetCurrentDayFileAsync()
        {
            StorageFile fileToReturn = null;
            var currentDayFile = await _localFolder.TryGetItemAsync(CurrentDayFileName);
            if (currentDayFile != null)
            {
                fileToReturn = (StorageFile)currentDayFile;
            }

            return fileToReturn;
        }

        public async Task ClearCurrentDayLogsAsync()
        {
            StorageFile currentDayLogsFile = await GetCurrentDayFileAsync();
            await ClearFileDataAsync(currentDayLogsFile);

        }

        private async Task ClearFileDataAsync(StorageFile fileToClear)
        {
            await SaveStringDataToFileAsync(fileToClear, "");
        }
    }
}
