﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaterPal.Services;

namespace WaterPal.UWP.Services
{
    public sealed class IoCService
    {
        private static Lazy<IoCService> _lazy =
            new Lazy<IoCService>(() => new IoCService());
        public static IoCService Instance = _lazy.Value;

        public IContainer Container { get; set; }

        private IoCService()
        {
            var builder = new ContainerBuilder();

            // Register types as Interfaces here
            //builder.RegisterType<ObjectType>().As<ObjectInterface>();
            Container = builder.Build();
        }
    }
}
