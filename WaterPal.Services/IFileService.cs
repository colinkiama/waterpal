﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using WaterPal.Model;

namespace WaterPal.Services
{
    public interface IFileService
    {
        Task SaveCurrentDayLogAsync(ObservableCollection<WaterLog> waterLogs);
        Task<ObservableCollection<WaterLog>> LoadCurrentDayLogAsync();
        Task ClearCurrentDayLogsAsync();
    }
}
