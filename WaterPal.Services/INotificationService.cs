﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WaterPal.Services
{
    public interface INotificationService
    {
        bool SetReminderNotification(TimeSpan reminderInterval);
        void SendGoalNotification();
        void SendMilestoneNotification();
    }
}
