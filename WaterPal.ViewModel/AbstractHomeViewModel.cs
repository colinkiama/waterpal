﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using WaterPal.Enums;
using WaterPal.Model;
using WaterPal.Services;
using WaterPal.Structs;

namespace WaterPal.ViewModel
{
    public abstract class AbstractHomeViewModel : Bindable
    {

        private Measurement _targetLogAmount;

        public Measurement TargetLogAmount
        {
            get { return _targetLogAmount; }
            set
            {
                _targetLogAmount = value;
                ProgressValue = RecalculateProgressValue();
            }
        }


        private double _progressValue;

        public double ProgressValue
        {
            get { return _progressValue; }
            set
            {
                _progressValue = value;
                NotifyPropertyChanged();
            }
        }




        private ObservableCollection<WaterLog> _currentDayLogs;

        public ObservableCollection<WaterLog> CurrentDayLogs
        {
            get { return _currentDayLogs; }
            set
            {
                _currentDayLogs = value;
                NotifyPropertyChanged();
            }
        }

        private Measurement _loggedWaterAmount;

        protected Measurement LoggedWaterAmount
        {
            get { return _loggedWaterAmount; }
            set
            {
                _loggedWaterAmount = value;
                LoggedWaterAmountString = $"{_loggedWaterAmount.Amount} " +
                    $"{_loggedWaterAmount.UnitOfMeasurement}";
                ProgressValue = RecalculateProgressValue();
            }
        }

        private string _loggedWaterAmountString;

        public string LoggedWaterAmountString
        {
            get { return _loggedWaterAmountString; }
            set
            {
                _loggedWaterAmountString = value;
                NotifyPropertyChanged();
            }
        }


        protected void CalculateInitialWaterLog(Unit defaultUnit)
        {
            Measurement initialWaterLog = new Measurement(0, defaultUnit);
            for (int i = 0; i < CurrentDayLogs.Count; i++)
            {
                initialWaterLog += CurrentDayLogs[i].LoggedAmount;
            }

            LoggedWaterAmount = initialWaterLog;
        }

        protected void AddLog(WaterLog logToAdd)
        {
            CurrentDayLogs.Add(logToAdd);
            // Now recalculate progress
        }

        private double RecalculateProgressValue()
        {
            double amountToReturn = 0;
            
            if (_loggedWaterAmount._universalAmount > 0 
                && _targetLogAmount._universalAmount > 0)
            {
                amountToReturn = _loggedWaterAmount._universalAmount /
                    _targetLogAmount._universalAmount * 100;
            }

            return amountToReturn;
        }


    }
}
