﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaterPal.Enums;
using WaterPal.Model;
using WaterPal.Structs;
using WaterPal.UWP.Services;
using WaterPal.ViewModel;
using Windows.UI.Xaml.Data;

namespace WaterPal.UWP.ViewModel
{
    public class HomeViewModel : AbstractHomeViewModel
    {
        public HomeViewModel()
        {

        }

        public async Task PerformAsyncStartupOperationsAsync()
        {
            //CurrentDayLogs = new System.Collections.ObjectModel.ObservableCollection<WaterLog>();
            //var loadedLogs = await FileIOService.Instance.LoadCurrentDayLogAsync();
            //if (loadedLogs != null)
            //{
            //    CurrentDayLogs = loadedLogs;
            //}

            // Test:
            await Task.Delay(3000);
            CurrentDayLogs = new ObservableCollection<WaterLog>()
            {
                new WaterLog(new Measurement(250, Unit.ml)),
                new WaterLog(new Measurement(45, Unit.ml)),
                new WaterLog(new Measurement(60, Unit.ml)),
                new WaterLog(new Measurement(79, Unit.ml)),
                new WaterLog(new Measurement(69, Unit.ml)),
            };
            CalculateInitialWaterLog(Unit.ml);
            TargetLogAmount = new Measurement(1000, Unit.ml);
        }

        private void RemoveLogs(IReadOnlyList<ItemIndexRange> selectedRanges)
        {
            List<WaterLog> logsToRemove = new List<WaterLog>();
            for (int i = 0; i < selectedRanges.Count; i++)
            {
                var range = selectedRanges[i];
                for (int j = range.FirstIndex; j < range.FirstIndex + range.Length; j++)
                {
                    logsToRemove.Add(CurrentDayLogs[j]);
                }
            }

            Measurement loggedWaterToRemove = new Measurement(0, Unit.ml);
            for (int i = 0; i < logsToRemove.Count; i++)
            {
                loggedWaterToRemove += logsToRemove[i].LoggedAmount;
                CurrentDayLogs.Remove(logsToRemove[i]);
            }

            LoggedWaterAmount -= loggedWaterToRemove;
        }



    }
}
